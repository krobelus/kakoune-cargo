Cargo integration for Kakoune
=============================

This package teaches [the Kakoune editor]
how to work with [Rust]'s build tool, [Cargo].

It provides the following features:

  - A `:cargo` command for running arbitrary cargo commands
  - The output of any Cargo command is routed to
    a special buffer named `*cargo*`
    and displayed in the [tools client], if one exists
  - The `*cargo*` buffer has syntax highlighting
    to approximate the pretty colours
    the Rust compiler uses on an interactive terminal
  - In the `*cargo*` buffer,
    hitting `<ret>` on anything that looks like an error message
    will open the associated file in the [jump client][tools client],
    with the cursor at the appropriate line and column,
    and the error message in a tooltip below
  - `:cargo-next-error` and `:cargo-previous-error` commands
    to cycle through all the errors found by
    the previous cargo invocation
  - A custom "cargo" mode gives single-key access
    to the most common cargo commands.

Installation
============

 1. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.
    
        mkdir -p ~/.config/kak/autoload
        
 2. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing
    
        :echo %val{runtime}/autoload
        
    inside Kakoune.
 3. Put a copy of `cargo.kak` inside
    the new autoload directory,
    such as by checking out this git repository:
    
        cd ~/.config/kak/autoload/
        git clone https://gitlab.com/Screwtapello/kakoune-cargo.git
        
 4. (optional) Add map a key to enter the custom "cargo" mode.
    You can map any key you want,
    but I map it to `c` in the user mode
    so it won't conflict with any standard mappings
    Kakoune might add in future.
    Put this in `~/.config/kak/kakrc`:
    
        map -docstring "Cargo commands" global user <c> \
            %{:enter-user-mode cargo<ret>}
            
    Then you can enter cargo mode with `,c`
    and see a menu of available cargo commands.

    You can add more commands yourself with:

        map -docstring "Run clippy" global cargo C \                                                                                                            
            %{: cargo clippy<ret>} 
    
Other Rust Tips
===============

Rust frequently includes `rustfmt`, the official Rust code formatting tool. You
can make Kakoune automatically format the current buffer when you write it to
disk by putting something like this in your `kakrc`:

    hook global WinSetOption filetype=rust %{
        # If rustfmt is available, let's set up formatting.
        evaluate-commands %sh{
            if command -v rustfmt >/dev/null 2>&1; then
                printf "%s\n" "set-option window formatcmd rustfmt"
                # If there's a config file here,
                # let's do formatting by default.
                if [ -f rustfmt.toml ]; then
                    printf "%s\n" "hook -group rust-auto-format window BufWritePre .* format"
                else
                    printf "%s\n" "echo -debug %{rustfmt.toml not found}"
                fi
            else
                printf "%s\n" "echo -debug %{rustfmt not available}"
            fi
        }

        hook global WinSetOption filetype=(?!rust).* %{
            remove-hooks window rust-auto-format
        }
    }

Note that the above example only enables automatic formatting if a
`rustfmt.toml` configuration file exists in the current directory, so you won't
accidentally reformat projects you work on that don't use `rustfmt`.

TODO
====

  - detect the presence/absence of cargo and warn if missing
      - the standard library does this for `jq` and some Go tools,
        copy what they do
  - detect the project root, for use with relative paths in error messages
      - `cargo locate-project` should be useful here
  - detect assertion failures from failing tests
      - Something like `assertion failure: .*? (filename):(line):(col)$`

[the Kakoune editor]: http://kakoune.org/
[Rust]: https://www.rust-lang.org/
[Cargo]: https://doc.rust-lang.org/cargo/guide/
[tools client]: https://github.com/mawww/kakoune/wiki/IDE
